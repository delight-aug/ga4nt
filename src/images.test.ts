import { Source } from "./background-script";
import { RandomImage, ImageMeta } from "./images";

describe("RandomImage", () => {
	it("returns every possible value before repeating itself", () => {
		const givenSources: Source[] = [
			{ title: "yamada", amount: 50 },
			{ title: "umi", amount: 50 },
			{ title: "totoro", amount: 50 },
		];

		const expected = givenSources.flatMap((src) => {
			const r = Array(src.amount);
			for (let i = 1; i <= src.amount; i++) {
				const filename = `${src.title}${String(i).padStart(3, "0")}.jpg`;
				r.push({
					title: src.title,
					filename,
					imageHref: `${undefined}/${filename.replaceAll("-", "")}`,
					upstreamHref: `${undefined}/${src.title}/#frame&gid=1&pid=${i}`,
				} as ImageMeta);
			}
			return r;
		});

		const actual = Array.from(new RandomImage(givenSources));

		expect(actual).toEqual(expect.arrayContaining(expected));
		expect(actual.length).toEqual(expected.length);
	});

	it("iterator is reusable", () => {
		const givenSources: Source[] = [
			{ title: "yamada", amount: 50 },
			{ title: "umi", amount: 50 },
			{ title: "totoro", amount: 50 },
		];

		const filenames = new RandomImage(givenSources);
		const first = Array.from(filenames);
		const second = Array.from(filenames);

		expect(second).toEqual(expect.arrayContaining(first));
		expect(second.length).toEqual(first.length);
	});
});
