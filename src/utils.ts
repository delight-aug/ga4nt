export class Repeater<T> implements Iterator<T> {
	private iterable;
	private iterator;

	constructor(iterable: Iterable<T>) {
		this.iterable = iterable;
		this.iterator = iterable[Symbol.iterator]();
	}

	next(): IteratorResult<T> {
		const { done, value } = this.iterator.next();
		if (done) {
			this.iterator = this.iterable[Symbol.iterator]();
			return this.next();
		}

		return { done, value };
	}
}

export class Fridge<T> implements Iterator<T> {
	private array: T[];
	private cursor: number = -1;
	private size: number;
	private update: () => Promise<T | void>;
	private trash?: T[];

	constructor(
		update: () => Promise<T | void>,
		array: T[] = [],
		size = 3,
		trash?: T[],
	) {
		this.array = array;
		this.size = size;
		this.update = update;
		this.trash = trash;

		let i = size - array.length;
		if (i === 0) return;
		for (let j = 0; j < i; j++)
			this.update().then((value) => {
				if (value) array.push(value);
			});
	}

	insert(el: T) {
		this.array.push(el);
		if (this.array.length >= this.size) {
			const v = this.array.shift();
			if (this.trash && v) this.trash.push(v);
			--this.cursor;
		}
	}

	advance(): T {
		if (this.cursor >= this.size - 1) this.cursor = -1;
		return this.array[++this.cursor];
	}

	next(): IteratorResult<T> {
		const value = this.advance();
		this.update().then((v) => {
			if (v === undefined) return;
			this.insert(v);
		});
		return { value };
	}
}

export class ReferenceCounter<I, S> {
	private refs: Map<I, S[]> = new Map();
	private trash?: I[];

	constructor(trash?: I[]) {
		this.trash = trash;
	}

	private toTrash(item: I) {
		this.refs.delete(item);
		if (this.trash) this.trash.push(item);
	}

	subscribe(item: I, subscriber: S) {
		const ids = this.refs.get(item);
		if (ids) {
			if (!ids.find((id) => id === subscriber)) ids.push(subscriber);
		} else {
			this.refs.set(item, [subscriber]);
		}
	}

	unsubscribe(subscriber: S) {
		for (let entries of this.refs.entries()) {
			const [item, ids] = entries;
			while (true) {
				const i = ids.findIndex((id) => id === subscriber);
				if (i === -1) break;
				ids.splice(i, 1);
			}
			if (ids.length === 0) this.toTrash(item);
		}
	}
}
