import { RandomFill } from "./random";

test("returns every possible value before repeating itself",
	() => {
		const start = 0;
		const end = 10;

		const expected = [];
		for (let i = start; i <= end; i++)
			expected.push(i);

		const actual = Array.from(new RandomFill(start, end));

		expect(actual).toEqual(
			expect.arrayContaining(expected)
		);
		expect(actual.length).toEqual(expected.length);
	}
);

test("iterator is reusable", () => {
	const filler = new RandomFill(0, 10);

	const first = Array.from(filler);
	const second = Array.from(filler);

	expect(second).toEqual(
		expect.arrayContaining(first)
	);
	expect(second.length).toEqual(first.length);
});
