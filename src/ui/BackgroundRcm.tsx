import browser from "webextension-polyfill";
import { useCallback } from "preact/hooks";
import * as styles from "./BackgroundRcm.module.scss";
import { ImageMeta } from "../images";

const _ = browser.i18n.getMessage;
const UPSTREAM_NAME = process.env.UPSTREAM_NAME;

export default function BackgroundRcm({ img, closeRcm }: {
	img: ImageMeta,
    closeRcm: () => void
}) {
	const onClick = useCallback(
		() => {
			closeRcm();
		},
		[closeRcm]
	);

	return (<div class={styles.root}>
		<h3>{_(img.title.replace("-", ""))}</h3>
		{/* rome-ignore lint/a11y/useValidAnchor: constistency with the following ignore */}
		<a href={img.upstreamHref} onClick={onClick}>{_("openOnURL", UPSTREAM_NAME)}</a>
		{/* rome-ignore lint/a11y/useValidAnchor: there is no good alternative way to donwload */}
		<a href={img.imageHref} download={img.filename} onClick={onClick}>{_("save")}</a>
	</div>);
}
