import { ComponentChildren } from "preact";
import { useMemo, useRef } from "preact/hooks";
import { createPortal } from "preact/compat";
import * as styles from "./RcmPortal.module.scss";

const OFFSET = 0;

export default function RcmPortal({children, cursorPos}: {
	children: ComponentChildren,
	cursorPos?: {pageX: number, pageY: number}
}) {
	const menuRef = useRef<HTMLDivElement>(null);
	const menuRect = menuRef.current?.getBoundingClientRect();
	const isOpen = menuRect && cursorPos;

	const pos = useMemo(() => {
		if (!(isOpen && menuRect && cursorPos)) return;

		const calc = (cursor: number, page: number, size: number) => {
			const space = page - cursor;

			if (space < (size + OFFSET))
				return cursor - size;
			else
				return cursor;
		};

		const top = calc(cursorPos.pageY, innerHeight, menuRect.height);
		const left = calc(cursorPos.pageX, innerWidth, menuRect.width);

		return {
			top,
			left
		};
	}, [menuRect, cursorPos]);

	return (createPortal(
		<div
			class={styles.portal}
			style={{
				visibility: isOpen ? "visible" : "hidden",
				...pos
			}}
			ref={menuRef}
		>
			{children}
		</div>,
		document.getElementById("context-menu-modal")!
	));
}
