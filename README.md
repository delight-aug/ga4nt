# Ghibli's Artworks for New Tab

This project is not affiliated with nor endorsed by Studio Ghibli. It makes use of images published by Studio Ghibli for free use within the bounds of common sense. In exchange, it provides links to the source of each image. It doesn't make any profit and is intended for entertainment purposes.

## Building

Install dependencies by running the `yarn` command.

Copy `.env.example` to `.env`, optionally modify it.

To build the extension, run:
```
yarn build
```

## Testing

Test by running `yarn test` command.

## Running

You can serve images locally from the `res` directory via a server that enables CORS for any origin, e.g.:
```
npx live-server --cors res
```

You can run the extension in Firefox without installing it using this command:
```
yarn fire
```

## Installation

To temporarily install it in Firefox — go to `about:debugging#/runtime/this-firefox`, click on "Load Temporary Add-on" and choose the `manifest.json` file from the `dist` directory. Note, that in this case Firefox will require the connection to resources to be over HTTPS.

For Chrome, go to the extensions page, turn on the `Developer mode` checkbox, click on `Load unpacked`, and choose the `dist` directory.
