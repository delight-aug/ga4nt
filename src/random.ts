export class RandomFill implements IterableIterator<number> {
	start: number;
	end: number;
	ranges: LeftoverRange[];

	constructor(start: number, end: number) {
		if (end < start) throw new Error("Starting index is higher than ending");
		this.start = start;
		this.end = end;
		this.ranges = [new LeftoverRange(start, end)];
	}

	[Symbol.iterator](): IterableIterator<number> {
		return new RandomFill(this.start, this.end);
	}

	next(): IteratorResult<number> {
		if (this.ranges.length === 0) return {
			done: true,
			value: null
		};

		const random = (to: number, from = 0) => {
			return Math.round(Math.random() * to) + from;
		};

		const rangeIndex = random(this.ranges.length - 1);
		const range = this.ranges[rangeIndex];
		const n = random(range.end - range.start, range.start);
		const newRanges = range.split(n);
		if (newRanges[0] != null)
			this.ranges.splice(rangeIndex, 1, newRanges[0]);
		else this.ranges.splice(rangeIndex, 1);
		if (newRanges[1] != null)
			this.ranges.splice(rangeIndex + 1, 0, newRanges[1]);

		return {
			done: false,
			value: n
		};
	}
}

class LeftoverRange {
	start: number;
	end: number;

	constructor(start: number, end: number) {
		this.start = start;
		this.end = end;
	}

	split(index: number): [LeftoverRange?, LeftoverRange?] {
		if (index < this.start || index > this.end) throw new Error("Index is out of the range");
		if (index === this.start && index === this.end) return [];
		const left = new LeftoverRange(this.start, index - 1);
		if (index === this.end) return [left];
		const right = new LeftoverRange(index + 1, this.end);
		if (index === this.start) return [right];
		return [left, right];
	}
}
